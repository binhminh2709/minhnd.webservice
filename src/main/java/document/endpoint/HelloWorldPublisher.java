package document.endpoint;

import javax.xml.ws.Endpoint;

import document.ws.HelloWorldImpl;

/**End point publisher*/
public class HelloWorldPublisher {
	
	public static void main(String[] args) {
		Endpoint.publish("http://localhost:9999/ws/hello?wsdl", new HelloWorldImpl());
		System.out.println("...........Start Webservice.....DOCUMENT");
	}
	
	/**
	 * Wrapper Class Package.Jaxws.MethodName Is Not Found. Have You Run APT To Generate Them
	 * http://www.mkyong.com/webservices/jax-ws/wrapper-class-package-jaxws-methodname-is-not-found-have-you-run-apt-to-generate-them/
	 * 
	 * cd ../minhnd.webservice/target/classes
	 * wsgen -keep -cp . com.minhnd.ws.HelloWorldImp
	 * */
}
