package rand;

import java.util.Random;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "rand.RandPublisher")
public class RandService {
	
	public static int	maxRands	= 16;
	
	@WebMethod	//Optional but helpful annonation
	public int next1() { return new Random().nextInt(); }
	
	@WebMethod	//Optional but helpful annonation
	public int[] nextN(final int n) {
		final int k = (n > maxRands) ? maxRands : Math.abs(n);
		int[] rands = new int[k];
		Random r = new Random();
		for (int i = 0; i < k; i++)
			rands[i] = r.nextInt();
		return rands;
	}
}
