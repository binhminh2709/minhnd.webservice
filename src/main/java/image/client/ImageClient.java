package image.client;

import image.ws.ImageServer;

import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class ImageClient {
	
	public static void main(String[] args) throws Exception {
		
		URL url = new URL("http://localhost:8888/ws/image?wsdl");
		QName qname = new QName("http://ws.image.minhnd.com/", "ImageServerImplService");
		
		Service service = Service.create(url, qname);
		ImageServer imageServer = service.getPort(ImageServer.class);
		
		/************  test download  ***************/
		Image image = imageServer.downloadImage("rss.png");
		
		//display it in frame
		JFrame frame = new JFrame();
		frame.setSize(300, 300);
		JLabel label = new JLabel(new ImageIcon(image));
		frame.add(label);
		frame.setVisible(true);
		
		System.out.println("START ....DOWNLOAD IMAGE ....downloadImage() : Download Successful!");
		
	}
	
}
