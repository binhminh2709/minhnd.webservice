The wsgen tool is used to parse an existing web service implementation class and
generates required files (JAX-WS portable artifacts) for web service deployment.

This wsgen tool is available in $JDK/bin folder.

2 common use cases for wsgen tool :
  -  Generates JAX-WS portable artifacts (Java files) for web service deployment.
  EX: wsgen -verbose -keep -cp . com.ws.ServerInfo
  
  -  Generates WSDL and xsd files, for testing or web service client development.
  EX: wsgen -verbose -keep -cp . com.ws.ServerInfo -wsdl
  
  
Reference:
  -  http://www.mkyong.com/webservices/jax-ws/jax-ws-wsgen-tool-example/

Huong dan:
1- Build project, de ra file class trong thư mục dỉr_project/bin
rồi thực hiện lệnh